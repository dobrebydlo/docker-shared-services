# docker-shared-services

Basic set of shared services for local development in docker-compose format

## What's inside

* Traefik for HTTP routing with nice readable host names
* MySQL as the universal data storage
* Redis for cache purposes

## How to use

* Clone this repo
* Run `docker compose up -d`
 